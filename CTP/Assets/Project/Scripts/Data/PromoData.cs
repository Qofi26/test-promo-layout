namespace RedPanda.Project.Data
{
    public class PromoData
    {
        public PromoType Type { get; }
        public string Title { get; }
        public PromoRarity Rarity { get; }
        public int Cost { get; }

        public string Icon()
        {
            // TODO: The icon name does not match the this code.
            // Now I have changed the method (add _), but can change the name of the sprite or use mapping
            return $"sprite_{Type.ToString().ToLower()}_{Rarity.ToString().ToLower()}";
        }

        public PromoData(string title, PromoType type, PromoRarity rarity, int cost)
        {
            Title = title;
            Type = type;
            Rarity = rarity;
            Cost = cost;
        }
    }

    public enum PromoRarity
    {
        Common,
        Rare,
        Epic
    }

    public enum PromoType
    {
        Chest,
        Special,
        InApp
    }
    
}