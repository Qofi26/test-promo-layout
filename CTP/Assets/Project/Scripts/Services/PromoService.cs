using System.Collections.Generic;
using RedPanda.Project.Data;
using RedPanda.Project.Interfaces;
using RedPanda.Project.Services.Interfaces;
using UnityEngine;

namespace RedPanda.Project.Services
{
    public sealed class PromoService : IPromoService
    {
        private List<IPromoModel> _promos = new();

        private readonly IUserService _userService;

        public PromoService(IUserService userService)
        {
            _userService = userService;

            var data = new List<PromoData>()
            {
                new ("Common \nchest", PromoType.Chest, PromoRarity.Common, 10),
                new ("Rare \nchest", PromoType.Chest, PromoRarity.Rare, 30),
                new ("Epic \nchest", PromoType.Chest, PromoRarity.Epic, 100),
                new ("Сommon \ninapp", PromoType.InApp, PromoRarity.Common, 15),
                new ("Сommon \ninapp", PromoType.InApp, PromoRarity.Common, 25),
                new ("Rare \ninapp", PromoType.InApp, PromoRarity.Rare, 65),
                new ("Common \nspec", PromoType.Special, PromoRarity.Common, 25),
                new ("Rare \nspec", PromoType.Special, PromoRarity.Rare, 100),
                new ("Common \nspec", PromoType.Special, PromoRarity.Common, 35),
                new ("Epic \nspec", PromoType.Special, PromoRarity.Epic, 40)
            };

            foreach (var promoData in data)
            {
                _promos.Add(new PromoModel<PromoData>(promoData));
            }
        }

        IReadOnlyList<IPromoModel> IPromoService.GetPromos()
        {
            return _promos;
        }

        public void BuyPromo(IPromoModel model)
        {
            if (!_promos.Contains(model))
            {
                Debug.LogError("Promo model not found");
                return;
            }

            var cost = model.Cost;
            if (!_userService.HasCurrency(cost))
            {
                Debug.LogError($"Not enough currency for buy promo {model.Title}");
                return;
            }

            _userService.ReduceCurrency(cost);
            Debug.Log($"Buy promo {model.Title}");
        }
    }
}