using System.Collections.Generic;
using RedPanda.Project.Interfaces;

namespace RedPanda.Project.Services.Interfaces
{
    public interface IPromoService
    {
        IReadOnlyList<IPromoModel> GetPromos();

        // TODO: Or can create special class for buy promo and other products
        void BuyPromo(IPromoModel model);
    }
}