using Grace.DependencyInjection;
using RedPanda.Project.Services;
using RedPanda.Project.Services.Interfaces;
using RedPanda.Project.Services.UI;
using RedPanda.Project.UI.Currency;
using RedPanda.Project.UI.Currency.Interfaces;
using RedPanda.Project.UI.Promo;
using RedPanda.Project.UI.Promo.Interfaces;
using UnityEngine;

namespace RedPanda.Project
{
    public sealed class Initializer : MonoBehaviour
    {
        private DependencyInjectionContainer _container = new();

        private void Awake()
        {
            _container.Configure(block =>
            {
                block.Export<UserService>().As<IUserService>().Lifestyle.Singleton();
                block.Export<PromoService>().As<IPromoService>().Lifestyle.Singleton();
                block.Export<UIService>().As<IUIService>().Lifestyle.Singleton();

                block.Export<CreditViewModel>().As<ICreditViewModel>();
                block.Export<PromoElementViewModel>().As<IPromoElementViewModel>();
            });

            _container.Locate<IUserService>();
            _container.Locate<IPromoService>();
            _container.Locate<IUIService>().Show("LobbyView");
        }
    }
}