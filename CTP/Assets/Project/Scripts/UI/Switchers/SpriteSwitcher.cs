﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace RedPanda.Project.UI.Switchers
{
    [Serializable]
    public sealed class SpriteSwitcher<TKey> : AbstractSwitcher<TKey, Sprite>
    {
        [SerializeField]
        private Image _target;

        protected override void SetValue(Sprite value)
        {
            _target.sprite = value;
        }
    }
}