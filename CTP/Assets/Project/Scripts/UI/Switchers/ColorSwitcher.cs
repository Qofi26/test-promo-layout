﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace RedPanda.Project.UI.Switchers
{
    [Serializable]
    public sealed class ColorSwitcher<TState> : AbstractSwitcher<TState, Color>
    {
        [SerializeField]
        private Graphic _target;

        protected override void SetValue(Color value)
        {
            _target.color = value;
        }
    }
}