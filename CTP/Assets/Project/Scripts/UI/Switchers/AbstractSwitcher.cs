﻿using System;
using UnityEngine;

namespace RedPanda.Project.UI.Switchers
{
    [Serializable]
    public abstract class AbstractSwitcher<TState, TValue>
    {
        [SerializeField]
        private Entry[] _entries = Array.Empty<Entry>();

        public void SetState(TState state)
        {
            foreach (var entry in _entries)
            {
                var isCurrentState = entry.State.Equals(state);
                if (isCurrentState)
                {
                    SetValue(entry.Value);
                }
            }
        }

        protected abstract void SetValue(TValue value);

        [Serializable]
        private class Entry
        {
            public TState State;
            public TValue Value;
        }
    }
}