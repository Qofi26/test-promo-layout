﻿using System;
using RedPanda.Project.Services.Interfaces;
using RedPanda.Project.UI.Currency.Interfaces;
using RedPanda.Project.UI.Utils;

namespace RedPanda.Project.UI.Currency
{
    public class CreditViewModel : ICreditViewModel, IDisposable
    {
        public ReactiveProperty<int> Amount { get; } = new();

        private readonly IUserService _userService;

        public CreditViewModel(IUserService userService)
        {
            _userService = userService;
            // TODO: Instead of event can use event manager
            _userService.OnCurrencyChanged += OnCurrencyChanged;
            OnCurrencyChanged(_userService.Currency);
        }

        private void OnCurrencyChanged(int amount)
        {
            Amount.SetValue(amount);
        }

        public void Dispose()
        {
            Amount?.Dispose();
            if (_userService != null)
            {
                _userService.OnCurrencyChanged -= OnCurrencyChanged;
            }
        }
    }
}