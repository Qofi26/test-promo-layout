﻿using RedPanda.Project.UI.Utils;

namespace RedPanda.Project.UI.Currency.Interfaces
{
    public interface ICurrencyViewModel
    {
        ReactiveProperty<int> Amount { get; }
    }
}