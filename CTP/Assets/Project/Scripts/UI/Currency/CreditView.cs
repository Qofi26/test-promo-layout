﻿using Grace.DependencyInjection.Attributes;
using RedPanda.Project.UI.Currency.Interfaces;
using RedPanda.Project.UI.Utils;
using TMPro;
using UnityEngine;

namespace RedPanda.Project.UI.Currency
{
    public class CreditView : View
    {
        [SerializeField]
        private TextMeshProUGUI _text;

        [SerializeField]
        private string _format = TextIconConstants.GEM_ICON + " {0}";

        private ICurrencyViewModel _viewModel;

        [Import]
        public void Inject(ICreditViewModel viewModel)
        {
            _viewModel = viewModel;
            _viewModel?.Amount.Subscribe(this, SetAmount);
        }

        // TODO: OnDestroy is not called when the view is destroyed as disabled.
        // If need use pool instead of destroy object, then use OnEnable + OnDisable 
        private void OnDestroy()
        {
            _viewModel?.Amount.Unsubscribe(this);
        }

        public void SetAmount(int amount)
        {
            _text.text = string.Format(_format, amount);
        }
    }
}