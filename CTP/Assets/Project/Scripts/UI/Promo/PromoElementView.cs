using Grace.DependencyInjection.Attributes;
using RedPanda.Project.Data;
using RedPanda.Project.UI.Promo.Interfaces;
using RedPanda.Project.UI.Switchers;
using RedPanda.Project.UI.Utils;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace RedPanda.Project.UI.Promo
{
    public class PromoElementView : View
    {
        [SerializeField]
        private Button _button;

        [SerializeField]
        private TextMeshProUGUI _title;

        [SerializeField]
        private Image _promoIcon;

        [SerializeField]
        private TextMeshProUGUI _cost;

        [SerializeField]
        private string _costFormat = TextIconConstants.GEM_ICON + "  x{0}";

        public IPromoElementViewModel ElementVieViewModel { get; private set; }

        [SerializeField]
        private SpriteSwitcher<PromoRarity> _backgroundSwitcher = new();

        [SerializeField]
        private ColorSwitcher<PromoRarity> _titleColorSwitcher = new();

        [Import]
        public void Inject(IPromoElementViewModel viewModel)
        {
            SetViewModel(viewModel);
        }

        public void SetViewModel(IPromoElementViewModel viewModel)
        {
            Unsubscribe();
            ElementVieViewModel = viewModel;
            Subscribe();
        }

        public void SetTitle(string title)
        {
            _title.text = title;
        }

        public void SetIcon(Sprite icon)
        {
            _promoIcon.sprite = icon;
        }

        public void SetRarity(PromoRarity rarity)
        {
            _backgroundSwitcher.SetState(rarity);
            _titleColorSwitcher.SetState(rarity);
        }

        public void SetCost(int cost)
        {
            _cost.text = string.Format(_costFormat, cost);
        }

        private void Awake()
        {
            _button.onClick.AddListener(OnClick);
        }

        private void OnDestroy()
        {
            Unsubscribe();
        }

        private void OnClick()
        {
            ElementVieViewModel?.BuyPromo();
        }

        private void Subscribe()
        {
            if (ElementVieViewModel == null)
            {
                return;
            }

            ElementVieViewModel.Title.Subscribe(this, SetTitle);
            ElementVieViewModel.Icon.Subscribe(this, SetIcon);
            ElementVieViewModel.Rarity.Subscribe(this, SetRarity);
            ElementVieViewModel.Cost.Subscribe(this, SetCost);
        }

        private void Unsubscribe()
        {
            if (ElementVieViewModel == null)
            {
                return;
            }

            ElementVieViewModel.Title.Unsubscribe(this);
            ElementVieViewModel.Icon.Unsubscribe(this);
            ElementVieViewModel.Type.Unsubscribe(this);
            ElementVieViewModel.Rarity.Unsubscribe(this);
            ElementVieViewModel.Cost.Unsubscribe(this);
        }
    }
}