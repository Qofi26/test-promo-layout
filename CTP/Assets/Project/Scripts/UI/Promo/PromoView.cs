﻿using System.Collections.Generic;
using Grace.DependencyInjection;
using Grace.DependencyInjection.Attributes;
using RedPanda.Project.Data;
using RedPanda.Project.Interfaces;
using RedPanda.Project.Services.Interfaces;
using RedPanda.Project.UI.Currency;
using RedPanda.Project.UI.Promo.Interfaces;
using RedPanda.Project.Utils;
using UnityEngine;

namespace RedPanda.Project.UI.Promo
{
    public class PromoView : View, IPromoElementEventsHandler
    {
        [SerializeField]
        private CreditView _creditView;

        // TODO: Can create special class for sections and create from prefab by data
        [SerializeField]
        private Transform _promoChestParent;

        [SerializeField]
        private Transform _promoSpecialParent;

        [SerializeField]
        private Transform _promoInAppParent;

        [SerializeField]
        private PromoElementView _promoElementPrefab;

        // TODO: Use string for resources path is bad decision 
        [SerializeField]
        private string _spriteAtlasName = "SpriteAtlases/PromoSpriteAtlas";

        private readonly PromoRarityComparer _promoRarityComparer = new();

        private readonly List<IPromoModel> _promoModels = new();

        private IPromoService _promoService;

        private void Start()
        {
            UpdateView();
        }

        [Import]
        public void Inject(IExportLocatorScope container, IPromoService promoService)
        {
            _promoService = promoService;
            container.Inject(_creditView);
        }

        [ContextMenu(nameof(UpdateView))]
        private void UpdateView()
        {
            _promoModels.Clear();
            _promoModels.AddRange(_promoService.GetPromos());
            _promoModels.Sort((x, y) => _promoRarityComparer.Compare(x.Rarity, y.Rarity));

            foreach (var promoModel in _promoModels)
            {
                var promoParent = promoModel.Type switch
                {
                    PromoType.Chest => _promoChestParent,
                    PromoType.Special => _promoSpecialParent,
                    PromoType.InApp => _promoInAppParent,
                    _ => _promoChestParent
                };

                var promoView = Instantiate(_promoElementPrefab, promoParent);
                Container.Inject(promoView);
                promoView.ElementVieViewModel.SetEventsHandler(this);
                promoView.ElementVieViewModel.SetModel(promoModel);
            }
        }

        void IPromoElementEventsHandler.BuyPromo(IPromoModel model)
        {
            _promoService.BuyPromo(model);
        }

        Sprite IPromoElementEventsHandler.GetSprite(string spriteName)
        {
            return ResourcesUtils.LoadSprite(_spriteAtlasName, spriteName);
        }
    }
}