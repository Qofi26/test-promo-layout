﻿using System;
using RedPanda.Project.Data;
using RedPanda.Project.Interfaces;
using RedPanda.Project.UI.Promo.Interfaces;
using RedPanda.Project.UI.Utils;
using UnityEngine;

namespace RedPanda.Project.UI.Promo
{
    public sealed class PromoElementViewModel : IDisposable, IPromoElementViewModel
    {
        private IPromoModel _model;
        private IPromoElementEventsHandler _eventsHandler;

        public ReactiveProperty<string> Title { get; } = new();
        public ReactiveProperty<Sprite> Icon { get; } = new();
        public ReactiveProperty<PromoType> Type { get; } = new();
        public ReactiveProperty<PromoRarity> Rarity { get; } = new();
        public ReactiveProperty<int> Cost { get; } = new();

        public void SetModel(IPromoModel model)
        {
            _model = model;
            Title.SetValue(model.Title);
            var icon = _eventsHandler?.GetSprite(model.GetIcon());
            Icon.SetValue(icon);
            Type.SetValue(model.Type);
            Rarity.SetValue(model.Rarity);
            Cost.SetValue(model.Cost);
        }

        public void SetEventsHandler(IPromoElementEventsHandler handler)
        {
            _eventsHandler = handler;
        }

        public void Dispose()
        {
            Title?.Dispose();
            Icon?.Dispose();
            Type?.Dispose();
            Rarity?.Dispose();
            Cost?.Dispose();
        }

        public void BuyPromo()
        {
            _eventsHandler?.BuyPromo(_model);
        }
    }
}