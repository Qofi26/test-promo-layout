﻿using RedPanda.Project.Data;
using RedPanda.Project.Interfaces;
using RedPanda.Project.UI.Utils;
using UnityEngine;

namespace RedPanda.Project.UI.Promo.Interfaces
{
    public interface IPromoElementViewModel
    {
        ReactiveProperty<string> Title { get; }
        ReactiveProperty<Sprite> Icon { get; }
        ReactiveProperty<PromoType> Type { get; }
        ReactiveProperty<PromoRarity> Rarity { get; }
        ReactiveProperty<int> Cost { get; }

        void BuyPromo();
        void SetModel(IPromoModel model);
        void SetEventsHandler(IPromoElementEventsHandler handler);
    }
}