﻿using RedPanda.Project.Interfaces;
using UnityEngine;

namespace RedPanda.Project.UI.Promo.Interfaces
{
    public interface IPromoElementEventsHandler
    {
        void BuyPromo(IPromoModel model);
        Sprite GetSprite(string spriteName);
    }
}