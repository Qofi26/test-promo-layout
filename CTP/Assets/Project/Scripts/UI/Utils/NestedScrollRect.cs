﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace RedPanda.Project.UI.Utils
{
    /// <summary>
    /// This class provide drag events to the parent object of the scroll rect.
    /// This is useful when you have a scroll rect inside another scroll rect, and you want to scroll the parent
    /// </summary>
    [RequireComponent(typeof(ScrollRect))]
    public class NestedScrollRect : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        [SerializeField]
        private float _deltaThreshold = 1f;

        private ScrollRect _scrollRect;
        private GameObject _target;

        private bool _initialState;
        private bool _isHorizontalDragging;
        private bool _isVerticalDragging;
        private PointerEventData _beginDragEventData;

        private void Awake()
        {
            TryGetComponent(out _scrollRect);
            _target = transform.parent.gameObject;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (_scrollRect.horizontal && _scrollRect.vertical)
            {
                return;
            }

            var delta = eventData.delta;

            if (!_isVerticalDragging && Mathf.Abs(delta.x) >= _deltaThreshold)
            {
                _isHorizontalDragging = true;
            }

            if (!_isHorizontalDragging && Mathf.Abs(delta.y) >= _deltaThreshold)
            {
                _isVerticalDragging = true;
            }

            // If we are dragging in the same direction as the scroll rect
            if (!_isHorizontalDragging && !_isVerticalDragging
                || _isHorizontalDragging && _scrollRect.horizontal
                || _isVerticalDragging && _scrollRect.vertical)
            {
                return;
            }

            _scrollRect.enabled = false;

            if (_beginDragEventData != null)
            {
                ExecuteEvents.ExecuteHierarchy(_target, _beginDragEventData,
                    ExecuteEvents.beginDragHandler);
                _beginDragEventData = null;
            }

            ExecuteEvents.ExecuteHierarchy(_target, eventData, ExecuteEvents.dragHandler);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            ExecuteEvents.ExecuteHierarchy(_target, eventData, ExecuteEvents.endDragHandler);

            _scrollRect.enabled = _initialState;
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            _isHorizontalDragging = false;
            _isVerticalDragging = false;

            _beginDragEventData = eventData;

            _initialState = _scrollRect.enabled;
        }
    }
}