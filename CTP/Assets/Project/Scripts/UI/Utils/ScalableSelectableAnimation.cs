﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace RedPanda.Project.UI.Utils
{
    [RequireComponent(typeof(Selectable))]
    public class ScalableSelectableAnimation : MonoBehaviour, IPointerDownHandler, IPointerUpHandler,
        IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField]
        private RectTransform _content;

        [SerializeField]
        private bool _animateOnClickOnly;

        [SerializeField]
        private ScalableSelectableAnimationPreset _preset;

        private Selectable _target;

        private Vector3 _initialScale;
        private bool _isFocused;
        private bool _isAnimating;
        private bool _isPressed;
        private Vector3 _targetScale;
        private float _time;
        private bool _increaseDirection;

        private bool Interactable => _target.IsInteractable();
        private bool PressedAndFocused => _isPressed && _isFocused;

        private float AnimationTime => _preset.AnimationTime;
        private float AnimationScale => _preset.AnimationScaleFactor;
        private AnimationCurve AnimationCurve => _preset.AnimationCurve;

        private void Awake()
        {
            TryGetComponent(out _target);
            if (_content == null)
            {
                _content = (RectTransform)transform;
            }

            _initialScale = _content.localScale;
        }

        private void Update()
        {
            Animate();
        }

        private void Animate()
        {
            if (!_isAnimating)
            {
                return;
            }

            if (!_animateOnClickOnly)
            {
                _increaseDirection = PressedAndFocused;
            }

            var delta = _increaseDirection
                ? Time.deltaTime
                : -Time.deltaTime;

            AnimateScale(delta);

            if (_time <= 0)
            {
                _isAnimating = false;
                _time = 0;
            }
        }

        private void AnimateScale(float delta)
        {
            var animationTime = Mathf.Max(AnimationTime, 0.01f);

            _time = Mathf.Clamp(_time + delta, 0f, animationTime);

            if (_animateOnClickOnly && _increaseDirection && _time >= animationTime)
            {
                _increaseDirection = false;
            }

            var ratio = Mathf.Clamp01(_time / animationTime);
            var t = AnimationCurve.Evaluate(ratio);
            _targetScale = Vector3.Lerp(_initialScale, _initialScale * AnimationScale, t);
            SetScale(_targetScale);
        }

        private void SetScale(Vector3 scale)
        {
            scale.z = 1f;
            if (_content.localScale != scale)
            {
                _content.localScale = scale;
            }
        }

        private void Play()
        {
            if (_isAnimating)
            {
                return;
            }

            _isAnimating = true;
        }

        #region IPointerHandler

        void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
        {
            if (Interactable && _animateOnClickOnly)
            {
                _increaseDirection = true;
                Play();
            }
        }

        void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
        {
            if (!Interactable)
            {
                return;
            }

            _isPressed = true;

            if (!_animateOnClickOnly)
            {
                Play();
            }
        }

        void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
        {
            _isFocused = true;
            if (_isPressed && !_animateOnClickOnly)
            {
                Play();
            }
        }

        void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
        {
            _isFocused = false;
        }

        void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
        {
            _isPressed = false;
        }

        #endregion
    }
}