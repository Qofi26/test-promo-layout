﻿using UnityEngine;

namespace RedPanda.Project.UI.Utils
{
    [CreateAssetMenu(menuName = "RedPanda/Preset/Animation/Scale Selectable Animation",
        fileName = "ScaleSelectableAnimation")]
    public class ScalableSelectableAnimationPreset : ScriptableObject
    {
        [SerializeField]
        private float _animationTime = 0.3f;

        [SerializeField]
        private AnimationCurve _animationCurve = AnimationCurve.EaseInOut(0, 0, 1f, 1f);

        [SerializeField]
        private float _animationScaleFactor = 0.9f;

        public float AnimationTime => _animationTime;
        public AnimationCurve AnimationCurve => _animationCurve;
        public float AnimationScaleFactor => _animationScaleFactor;
    }
}