﻿using System;
using System.Collections.Generic;

namespace RedPanda.Project.UI.Utils
{
    public class ReactiveProperty<T> : IDisposable
    {
        public T Value { get; private set; }

        private readonly List<ActionWrap> _actions = new List<ActionWrap>(0);

        public ReactiveProperty()
        {
        }

        public ReactiveProperty(T value)
        {
            SetValue(value);
        }

        public void SetValue(T value)
        {
            if (!EqualityComparer<T>.Default.Equals(Value, value))
            {
                Value = value;
                Notify();
            }
        }

        public ReactiveProperty<T> Subscribe(object owner, Action<T> action)
        {
            _actions.Add(new ActionWrap { Action = action, Owner = owner });
            action?.Invoke(Value);
            return this;
        }

        public void Unsubscribe(object owner)
        {
            _actions.RemoveAll(e => e.Owner == owner);
        }

        public void Notify()
        {
            _actions.RemoveAll(e => e.Owner == null);
            foreach (var action in _actions)
            {
                action.Action?.Invoke(Value);
            }
        }

        public void Dispose()
        {
            _actions.Clear();
        }

        private class ActionWrap
        {
            public Action<T> Action;
            public object Owner;
        }

        public static implicit operator T(ReactiveProperty<T> p)
        {
            return p.Value;
        }

        public static implicit operator ReactiveProperty<T>(T value)
        {
            return new ReactiveProperty<T>(value);
        }
    }
}