using UnityEngine;
using UnityEngine.UI;

namespace RedPanda.Project.UI
{
    public sealed class LobbyView : View
    {
        [SerializeField]
        private Button _startButton;

        // TODO: Resources used string path. This is not ideal.
        // Can create a separate manager for management or at least use AssetReference to save the path
        [SerializeField]
        private string _promoScreenName = "PromoView";

        private void Awake()
        {
            _startButton.onClick.AddListener(OnClickStart);
        }

        private void OnClickStart()
        {
            UIService.Close(name);
            UIService.Show(_promoScreenName);
        }
    }
}