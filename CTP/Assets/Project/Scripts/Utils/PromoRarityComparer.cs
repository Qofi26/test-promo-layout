﻿using System.Collections.Generic;
using RedPanda.Project.Data;

namespace RedPanda.Project.Utils
{
    public class PromoRarityComparer : IComparer<PromoRarity>
    {
        public int Compare(PromoRarity x, PromoRarity y)
        {
            if (x == PromoRarity.Epic && y != PromoRarity.Epic)
                return -1;
            if (x != PromoRarity.Epic && y == PromoRarity.Epic)
                return 1;

            if (x == PromoRarity.Rare && y == PromoRarity.Common)
                return -1;
            if (x == PromoRarity.Common && y == PromoRarity.Rare)
                return 1;

            return 0;
        }
    }
}