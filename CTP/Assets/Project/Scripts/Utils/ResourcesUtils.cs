﻿using UnityEngine;
using UnityEngine.U2D;

namespace RedPanda.Project.Utils
{
    public static class ResourcesUtils
    {
        public static SpriteAtlas LoadSpriteAtlas(string spriteAtlasName)
        {
            return Resources.Load<SpriteAtlas>(spriteAtlasName);
        }

        public static Sprite LoadSprite(string spriteAtlasName, string spriteName)
        {
            var atlas = LoadSpriteAtlas(spriteAtlasName);
            return atlas.GetSprite(spriteName);
        }
    }
}